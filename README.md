# Create new function

```
./create.sh {function-name}
```
You will choose the trigger option.
<br/>
It will fill automatically deployment.json with corresponding configuration.
<br/>
And generate a new directory with the given name and Node.js setup for the selected trigger.

# Deployment

```
./deploy.sh [...{function-name}]
```
Use deployment.json to deploy function to GCP.
<br/>
Specify names as arguments to deploy specified functions only.


# Structure
All functions must be declared in its own directory named by the GCP identifier and reflected in package.json as project name
<br/>
For deployment function must be referenced by deployment.json with at least a trigger property.
<br/>
If function require environment variables it can be declared in a file located in sub folder environments/gcp.yaml.
<br/>
