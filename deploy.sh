#!/bin/bash

deploymentFile="./deployment.json"

function onMessageArgs() {
  name=$1
  local args="--entry-point onMessage --trigger-topic ${name}-topic"

  echo "${args}"
  return 0
}

function onRequestArgs() {
  name=$1
  local args="--entry-point onRequest --trigger-http"

  isPublic=$(jq -r .[\""${name}"\"].onRequest.isPublic ${deploymentFile})
  if [[ $isPublic = "true" ]]; then
    args+=" --allow-unauthenticated"
  fi

  echo "${args}"
  return 0
}

function triggerArgs {
  trigger=$(jq -r .[\""${name}"\"].trigger ${deploymentFile})
  case ${trigger} in
    "onMessage")
      result=$(onMessageArgs "$name")
      errorCode=$?
      ;;
    "onRequest")
      result=$(onRequestArgs "$name")
      errorCode=$?
      ;;
    *)
      echo "invalid trigger for ${name} function ${trigger}"
      return 1
      ;;
  esac
  echo "$result"
  return $errorCode
}

function deploy {
  name=$1

  # Must be node.js runtime
  if [[ ! -f "${name}/package.json" ]]; then
    echo "ignore deployment for ${name} no package.json found"
    return 1
  fi

  # shellcheck disable=SC2164
  cd "${name}" && npm i && cd -

  local command="gcloud functions deploy ${name} --quiet --region=europe-west1 --runtime nodejs12 --source ${name}"

  # Add env if defined
  envFile="${name}/environments/gcp.yaml"
  if [[ -f ${envFile} ]]; then
    command+=" --env-vars-file ${envFile}"
  fi

  # Add trigger configuration
  result=$(triggerArgs "$name")
  errorCode=$?
  if [[ $errorCode -ne 0 ]]; then
    echo "$result"
    return $errorCode
  fi
  command+=" $result"

  # Execute command
  ${command}
}


# Require deployment.json file
if [[ ! -f ${deploymentFile} ]]; then
  echo "Missing file ${deploymentFile}"
  exit 1;
fi

# Read all deployable functions
functions=()
while IFS='' read -r line; do
  functions+=("$line")
done < <(jq -r 'keys[]' deployment.json)

# Filter by arguments (all if none specified)
if [[ $# -gt 0 ]]; then
  for idx in "${!functions[@]}"; do
    if [[ ! $* =~ ${functions[idx]} ]]; then
      unset 'functions[idx]'
    fi
  done
fi

# Deploy for each function
for func in "${functions[@]}"; do
  echo "Deploying function ${func}"
  result=$(deploy "${func}")
  echo "$result"
done
