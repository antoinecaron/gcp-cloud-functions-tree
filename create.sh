#!/bin/bash

function writeCloudIgnore {
  {
    echo "# Ignore everything by default"
    echo "*"
    echo ""
    echo "# Allow files explicitly"
    echo "!index.js"
    echo "!package.json"
    echo "!package-lock.json"
    echo ""
    echo "# Explicitly allow current dir. \`gcloud deploy\` fails without it."
    echo "!."
  } > .gcloudignore
}

function triggerEntryPoint {
  local trigger=${1}
  case $1 in
  "onRequest")
    echo "	async ${trigger}(req, res) {"
    echo "		try {"
    echo "			const result = await ${functionName}(Object.assign({}, req.query, req.body));"
    echo "			res.send(result);"
    echo "		} catch (error) {"
    echo "			res.status(400).send(error.message || JSON.stringify(error));"
    echo "		}"
    echo "	}"
    ;;
  "onMessage")
    echo "	async ${trigger}(message) {"
    echo "		const buffer = Buffer.from(message.data, 'base64');"
    echo "		const payload = JSON.parse(buffer.toString());"
    echo "		await ${functionName}(payload);"
    echo "	}"
    ;;
  esac
}

function writeIndex {
  local trigger=${2}
  functionName=$(gsed -r 's/(-)(\w)/\U\2/g' <<< "${1}")
  {
    echo "async function ${functionName}(payload) {"
    echo "	// TODO"
    echo "}"
    echo ""
    echo "module.exports = {"
    triggerEntryPoint "${trigger}"
    echo "};"
  } > index.js
}

function writeDeployment {
  local content="{}"
  if [[ -f deployment.json ]];then
    content=$(cat deployment.json)
  fi
  jq ". + {\"${1}\":{\"trigger\":\"${2}\"}}" <<< $content > deployment.json
}



echo "Which trigger ?"
options=("onMessage" "onRequest")
select trigger in "${options[@]}"; do
  case ${trigger} in
    "onMessage" | "onRequest") ;;
    *) continue;;
  esac
  break
done

name=$1
writeDeployment "${name}" "${trigger}"

mkdir -p "${name}"
cd "${name}" || exit
writeIndex "${name}" "${trigger}"
writeCloudIgnore
npm init -y
